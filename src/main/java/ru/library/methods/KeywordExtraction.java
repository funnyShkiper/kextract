package ru.library.methods;

import java.util.List;

import ru.library.text.word.Word;

public interface KeywordExtraction {

    List<Word> extractKeywords(List<Word> words, Integer numberOfKeywords) throws Exception;

}
