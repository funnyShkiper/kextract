package ru.library.methods;

import ru.library.text.word.Word;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface KeywordExtractionFromDocumentList {

    public Map<String, List<Word>> extractKeywords (HashMap<String, List<Word>> documents, Integer numberOfKeywords) throws IOException;

}
