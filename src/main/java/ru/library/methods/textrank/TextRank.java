package ru.library.methods.textrank;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.library.methods.KeywordExtraction;
import ru.library.text.word.Word;

/**
 * Реализация классического алгоритма TextRank по извлечению ключевых слов из текста.
 */
public class TextRank implements KeywordExtraction {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    //Фактор затухания, стандартно установлено 0.85
    static final float d = 0.85f;

    //Максимально число итераций
    static final int maxIter = 200;

    static final float minDiff = 0.0001f;

    //Размер окна сканирования текста, стандартно равно 3
    private int windowSize = 3;

    private List<Word> words;

    public TextRank() {
    }

    public TextRank(List<Word> words) {
        this.words = words;
    }

    /**
     * Метод получения ключевых слов частотным методом
     * @param words список слов текста предоставляемых для обработки
     * @param numberOfKeywords количество возращаемых ключевых слов
     * @return список слов извлеченных при помощи метода TextRank
     */
    @Override
    public List<Word> extractKeywords(List<Word> words, Integer numberOfKeywords) throws IOException {
        this.words = words;

        List<String> termList = new ArrayList<>();
        for (Word word : words) {
            termList.add(word.getWord());
        }

        Map<String, Float> score = getWordScore(termList);

        //rank keywords in terms of their score
        List<Map.Entry<String, Float>> entryList = new ArrayList<>(score.entrySet());
        Collections.sort(entryList, (o1, o2) -> (o1.getValue() - o2.getValue() > 0 ? -1 : 1));

        List<Word> resultList = new ArrayList<>();
        for (Map.Entry<String, Float> entry : entryList) {
            for (Word word : words) {
                if (entry.getKey().equals(word.getWord())) {
                    Word wordInTextRank = new Word(
                            word.getWord(), word.getPartOfSpeech(), word.getMorphCharateristics(), entry.getValue()
                    );
                    resultList.add(wordInTextRank);
                    break;
                }
            }
        }

        List<Word> keywords = new ArrayList<>();

        if (numberOfKeywords == null) {
            for (int i = 0; i < resultList.size(); i++) {
                keywords.add(resultList.get(i));
            }
            return keywords;
        }

        for (int i = 0; i < numberOfKeywords; i++) {
            try {
                keywords.add(resultList.get(i));
            } catch (IndexOutOfBoundsException e) {
                logger.error("Количество требуемых слов превышает объем доступных.");
                break;
            }
        }
        return keywords;
    }

    /**
     * Функция подсчета веса каждого слова по классическому алгоритму TextRank
     */
    public Map<String, Float> getWordScore(List<String> termList) throws IOException {

        int count = 1;
        Map<String, Integer> wordPosition = new HashMap<>();
        List<String> wordList = new ArrayList<>();

        for (String word : termList) {
            wordList.add(word);
            if (!wordPosition.containsKey(word)) {
                wordPosition.put(word, count);
                count++;
            }
        }

        // генерация графа текста
        Map<String, Set<String>> words = new HashMap<>();
        Queue<String> que = new LinkedList<>();
        for (String w : wordList) {
            if (!words.containsKey(w)) {
                words.put(w, new HashSet<String>());
            }
            que.offer(w);
            if (que.size() > windowSize) {
                que.poll();
            }

            for (String w1 : que) {
                for (String w2 : que) {
                    if (w1.equals(w2)) {
                        continue;
                    }
                    words.get(w1).add(w2);
                    words.get(w2).add(w1);
                }
            }
        }


        Map<String, Float> score = new HashMap<>();
        for (int i = 0; i < maxIter; ++i) {
            Map<String, Float> m = new HashMap<>();
            float maxDiff = 0;
            for (Map.Entry<String, Set<String>> entry : words.entrySet()) {

                String key = entry.getKey();
                Set<String> value = entry.getValue();
                m.put(key, 1 - d);
                for (String other : value) {
                    int size = words.get(other).size();
                    if (key.equals(other) || size == 0) {
                        continue;
                    }
                    m.put(key, m.get(key) + d / size * (score.get(other) == null ? 0 : score.get(other)));
                }
                maxDiff = Math.max(maxDiff, Math.abs(m.get(key) - (score.get(key) == null ? 1 : score.get(key))));
            }
            score = m;
            if (maxDiff <= minDiff)
                break;
        }
        return score;
    }

    public void setWindowSize(int windowSize) {
        this.windowSize= windowSize;
    }
}
