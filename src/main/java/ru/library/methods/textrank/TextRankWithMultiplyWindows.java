package ru.library.methods.textrank;

import ru.library.methods.KeywordExtraction;
import ru.library.text.word.Word;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Модификация классического алгоритма выделения ключевых слов TextRank.
 * Данная модификация отличается построение графа текста при помощи переменной ширины окна обзора текста.
 */
public class TextRankWithMultiplyWindows implements KeywordExtraction {

    private int minWindow = 3;
    private int maxWindow = 5;

    public TextRankWithMultiplyWindows() {
    }

    public TextRankWithMultiplyWindows(int minWindow, int maxWindow) {
        this.minWindow = minWindow;
        this.maxWindow = maxWindow;
    }

    public void setWindows(int minWindow, int maxWindow) {
        this.minWindow = minWindow;
        this.maxWindow = maxWindow;
    }

    @Override
    public List<Word> extractKeywords(List<Word> words, Integer numberOfKeywords) throws IOException {

        List<Word> tempKeywordScore;
        List<Word> allKeywordScore = new ArrayList<>();

        for (int i = minWindow; i <= maxWindow; i++) {

            TextRank textRank = new TextRank();
            textRank.setWindowSize(i);

            tempKeywordScore = textRank.extractKeywords(words, words.size());

            for (Word word : tempKeywordScore) {
                if (allKeywordScore.size() == 0) {
                    allKeywordScore = tempKeywordScore;
                    break;
                }
                boolean put = false;
                for (Word item : allKeywordScore) {
                    if (word.getWord().equals(item.getWord())) {
                        item.setScope(item.getScope() + word.getScope());
                        put = true;
                        break;
                    }
                }
                if (!put) {
                    allKeywordScore.add(word);
                }
            }
        }

        Collections.sort(allKeywordScore, new Comparator<Word>() {
            @Override
            public int compare(Word o1, Word o2) {
                return o2.getScope() > o1.getScope() ? 1 : -1;
            }
        });

        List<Word> keywords = new ArrayList<>();

        if (numberOfKeywords == null) {
            for (int i = 0; i < allKeywordScore.size(); i++) {
                keywords.add(allKeywordScore.get(i));
            }
            return keywords;
        }

        try {
            for (int i = 0; i < numberOfKeywords; i++) {
                keywords.add(allKeywordScore.get(i));
            }
        } catch (Exception ex) {
            return keywords;
        }
        return keywords;
    }
}
