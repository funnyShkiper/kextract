package ru.library.methods.textrank;

import ru.library.methods.KeywordExtractionFromDocumentList;
import ru.library.methods.tfidf.TFIDF;
import ru.library.text.word.Word;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Метод извслечения ключевых слов {@link TextRankWithIDF} является гибридной смесью метода {@link TextRank} <br>
 * и метода {@link TFIDF}. Данный метод применяется для корпуса текстов, в ином случае целесообразность данного метода теряется.
 */
public class TextRankWithIDF implements KeywordExtractionFromDocumentList {

    private HashMap<String, List<Word>> documents;

    public TextRankWithIDF() {
    }

    /**
     * Метод извлечения ключевых слов из каждого документа списка
     *
     * @param documents        список документов для обработки
     * @param numberOfKeywords количество возращаемых ключевых слов
     * @return map текстов с ключевыми словами, где key - название текста, value - список ключевых слов
     * @throws IOException
     */
    @Override
    public Map<String, List<Word>> extractKeywords (HashMap<String, List<Word>> documents, Integer numberOfKeywords) throws IOException {

        this.documents = documents;

        Map<String, List<Word>> keywords = textRankMultiplyIDF();
        for (String title : keywords.keySet()) {
            for (Word word : keywords.get(title)) {
                for (int i = 0; i < documents.get(title).size(); i++) {
                    Word tmp = documents.get(title).get(i);
                    if (word.getWord().equals(tmp.getWord())) {
                        documents.get(title).get(i).setScope(word.getScope());
                        break;
                    }
                }
            }
        }

        for (String title : documents.keySet()) {
            List<Word> list = documents.get(title).stream().sorted(Comparator.comparing(Word::getScope).reversed()).collect(Collectors.toList());
            List<Word> tmp = new ArrayList<>();
            for (int i = 0; i < numberOfKeywords; i++) {
                try {
                    if (list.get(i).getScope() > 0) {
                        tmp.add(list.get(i));
                    }
                } catch (Exception ex) {
                    break;
                }
            }
            documents.put(title, tmp);
        }
        return documents;
    }

    /**
     * Подсчет коэффициентов TeaxtRank и IDF.
     * @return
     * @throws IOException
     */
    private Map<String, List<Word>> textRankMultiplyIDF() throws IOException {

        Map<String, List<Word>> result = new HashMap<>();

        TFIDF tfidf = new TFIDF(documents);

        //get IDF values for the words of the documents
        Map<String, Float> idfForDocuments = tfidf.idfForTexts();

        for (String title : documents.keySet()) {
            TextRank textRank = new TextRank(documents.get(title));

            List<String> termList = new ArrayList<>();
            for (Word word : documents.get(title)) {
                termList.add(word.getWord());
            }

            Map<String, Float> trKeywords = textRank.getWordScore(termList);
            Iterator<Map.Entry<String, Float>> it = trKeywords.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry<String, Float> temp = it.next();
                String key = temp.getKey();
                trKeywords.put(key, temp.getValue() * idfForDocuments.get(key));
            }

            //sort the words in terms of their score in descending order
            List<Map.Entry<String, Float>> entryList = new ArrayList<>(trKeywords.entrySet());
            Collections.sort(entryList, (o1, o2) -> o2.getValue().compareTo(o1.getValue()));

            List<Word> temp = new ArrayList<>();
            for (Map.Entry<String, Float> item : entryList) {
                Word word = new Word(item.getKey(), item.getValue());
                temp.add(word);
            }
            result.put(title, temp);
        }
        return result;
    }
}
