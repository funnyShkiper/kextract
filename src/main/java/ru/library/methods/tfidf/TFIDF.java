package ru.library.methods.tfidf;

import ru.library.methods.KeywordExtractionFromDocumentList;
import ru.library.text.word.Word;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class TFIDF implements KeywordExtractionFromDocumentList {

    public HashMap<String, List<Word>> documents;

    public TFIDF(HashMap<String, List<Word>> documents) {
        this.documents = documents;
    }

    public TFIDF() {

    }

    @Override
    public Map<String, List<Word>> extractKeywords(HashMap<String, List<Word>> documents, Integer numberOfKeywords) throws IOException {

        this.documents = documents;

        Map<String, List<Word>> keywords = getKeywords();
        for (String title : keywords.keySet()) {
            for (Word word : keywords.get(title)) {
                for (int i = 0; i < documents.get(title).size(); i++) {
                    Word tmp = documents.get(title).get(i);
                    if (word.getWord().equals(tmp.getWord())) {
                        documents.get(title).get(i).setScope(word.getScope());
                        break;
                    }
                }
            }
        }

        for (String title : documents.keySet()) {
            List<Word> list = documents.get(title).stream().sorted(Comparator.comparing(Word::getScope).reversed()).collect(Collectors.toList());
            List<Word> tmp = new ArrayList<>();
            for (int i = 0; i < numberOfKeywords; i++) {
                try {
                    if (list.get(i).getScope() > 0) {
                        tmp.add(list.get(i));
                    }
                } catch (Exception ex) {
                    break;
                }
            }
            documents.put(title, tmp);
        }
        return documents;
    }

    private List<String> convertListToString(List<Word> words) {
        List<String> list = new ArrayList<>();
        for (Word word : words) {
            list.add(word.getWord());
        }
        return list;
    }

    /**
     * calculate TF value of each word in terms of the content of file
     * @return
     */
    private HashMap<String, Float> getTF(List<Word> text) throws IOException {
        List<String> words = convertListToString(text);
        long wordLen = text.size();

        //get TF values
        HashMap<String, Integer> wordCount = new HashMap<>();
        HashMap<String, Float> TFValues = new HashMap<>();

        for (String word : words) {
            if (wordCount.get(word) == null) {
                wordCount.put(word, 1);
            } else {
                wordCount.put(word, wordCount.get(word) + 1);
            }
        }
        Iterator<Map.Entry<String, Integer>> iter = wordCount.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry<String, Integer> entry = iter.next();
            TFValues.put(entry.getKey().toString(), Float.parseFloat(entry.getValue().toString()) / wordLen);
        }
        return TFValues;
    }

    /**
     * calculate TF values for each word of each selected text
     * @return
     * @throws IOException
     */
    private HashMap<String, HashMap<String, Float>> tfForTexts() throws IOException {
        HashMap<String, HashMap<String, Float>> allTF = new HashMap<>();

        for (String text : documents.keySet()) {
            HashMap<String, Float> dict = new HashMap<>();
            dict = getTF(documents.get(text));
            allTF.put(text, dict);
        }
        return allTF;
    }

    /**
     * calculate IDF values for each word  under a TestTextList
     * @return(HashMap<String, Float>): "word:IDF Value" pairs
     */
    public HashMap<String, Float> idfForTexts() throws IOException {
        int docNum = documents.size();

        Map<String, Set<String>> passageWords = new HashMap<>();
        //get words that are not repeated of a file
        for (String text : documents.keySet()) {

            //TokenizerText tokenizerText = new TokenizerText(text.getText());
            List<String> terms = convertListToString(documents.get(text));

            Set<String> words = new HashSet<>();
            for (String term : terms) {
                words.add(term);
            }
            passageWords.put(text, words);
        }

        //get IDF values
        HashMap<String, Integer> wordPassageNum = new HashMap<>();
        for (String text : documents.keySet()) {

            Set<String> wordSet = new HashSet<>();
            wordSet = passageWords.get(text);
            for (String word : wordSet) {
                if (wordPassageNum.get(word) == null) {
                    wordPassageNum.put(word, 1);
                } else {
                    wordPassageNum.put(word, wordPassageNum.get(word) + 1);
                }
            }

        }

        HashMap<String, Float> wordIDF = new HashMap<>();
        Iterator<Map.Entry<String, Integer>> iterDict = wordPassageNum.entrySet().iterator();
        while (iterDict.hasNext()) {
            Map.Entry<String, Integer> entry = iterDict.next();
            float value = (float) Math.log(docNum / (Float.parseFloat(entry.getValue().toString())));
            wordIDF.put(entry.getKey().toString(), value);
        }
        return wordIDF;
    }

    /**
     * calculate TF-IDF value for each word of each text
     * @return
     * @throws IOException
     */
    private Map<String, HashMap<String, Float>> getTextsTFIDF() throws IOException {

        HashMap<String, HashMap<String, Float>> textsTF = new HashMap<>();
        HashMap<String, Float> textsIDF = new HashMap<>();

        textsTF = tfForTexts();
        textsIDF = idfForTexts();

        Map<String, HashMap<String, Float>> textsTFIDF = new HashMap<>();
        Map<String, Float> singlePassageWord = new HashMap<>();

        for (String text : documents.keySet()) {

            HashMap<String, Float> temp = new HashMap<>();
            singlePassageWord = textsTF.get(text);

            Iterator<Map.Entry<String, Float>> it = singlePassageWord.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry<String, Float> entry = it.next();
                String word = entry.getKey();
                Float TFIDF = entry.getValue() * textsIDF.get(word);
                temp.put(word, TFIDF);
            }
            textsTFIDF.put(text, temp);
        }
        return textsTFIDF;
    }

    public Map<String, List<Word>> getKeywords() throws IOException {

        // calculate TF-IDF value for each word of each text
        Map<String, HashMap<String, Float>> textsTFIDF = new HashMap<>();
        textsTFIDF = getTextsTFIDF();

        Map<String, List<Word>> keywordsOfTheTexts = new HashMap<>();
        for (String text : documents.keySet()) {

            Map<String, Float> singlePassageTFIDF = new HashMap<>();
            singlePassageTFIDF = textsTFIDF.get(text);

            List<Word> result = new ArrayList<>();
            for (String key : singlePassageTFIDF.keySet()) {
                result.add(new Word(key, singlePassageTFIDF.get(key)));
            }

            result = result.stream().sorted(Comparator.comparing(Word::getScope).reversed()).collect(Collectors.toList());

            //sort the keywords in terms of TF-IDF value in descending order
            List<Map.Entry<String, Float>> entryList = new ArrayList<>(singlePassageTFIDF.entrySet());

            Collections.sort(entryList, new Comparator<Map.Entry<String, Float>>() {
                @Override
                public int compare(Map.Entry<String, Float> o1, Map.Entry<String, Float> o2) {
                    return o2.getValue().compareTo(o1.getValue());
                }
            });

            //get keywords
            List<String> keywordList = new ArrayList<>();
            for (int k = 0; k < 15; k++) {
                try {
                    keywordList.add(entryList.get(k).getKey());
                } catch (IndexOutOfBoundsException ex) {
                    continue;
                }
            }
            keywordsOfTheTexts.put(text, result);
        }
        return keywordsOfTheTexts;
    }
}
