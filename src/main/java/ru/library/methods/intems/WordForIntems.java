package ru.library.methods.intems;

import ru.library.text.word.Word;

public class WordForIntems extends Word {

    private float fWeight;
    private float qWeigth;

    private float frequency;

    private int numberOfWordsMoreThenThisLength;
    private int numberOfWordsMoreThenThisFrequency;

    private int length;

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public WordForIntems() {
    }

    public WordForIntems(String word, byte partOfSpeech, long morphCharateristics) {
        super(word, partOfSpeech, morphCharateristics);
    }

    public WordForIntems(String word, byte partOfSpeech, long morphCharateristics, float fWeight, float qWeigth) {
        super(word, partOfSpeech, morphCharateristics);
        this.fWeight = fWeight;
        this.qWeigth = qWeigth;
        super.setScope(this.qWeigth - this.qWeigth);
    }

    public float getfWeight() {
        return fWeight;
    }

    public void setfWeight(float fWeight) {
        this.fWeight = fWeight;
    }

    public float getqWeigth() {
        return qWeigth;
    }

    public void setqWeigth(float qWeigth) {
        this.qWeigth = qWeigth;
    }

    public float getFrequency() {
        return frequency;
    }

    public void setFrequency(float frequency) {
        this.frequency = frequency;
    }

    public int getNumberOfWordsMoreThenThisLength() {
        return numberOfWordsMoreThenThisLength;
    }

    public void setNumberOfWordsMoreThenThisLength(int numberOfWordsMoreThenThisLength) {
        this.numberOfWordsMoreThenThisLength = numberOfWordsMoreThenThisLength;
    }

    public int getNumberOfWordsMoreThenThisFrequency() {
        return numberOfWordsMoreThenThisFrequency;
    }

    public void setNumberOfWordsMoreThenThisFrequency(int numberOfWordsMoreThenThisFrequency) {
        this.numberOfWordsMoreThenThisFrequency = numberOfWordsMoreThenThisFrequency;
    }


}
