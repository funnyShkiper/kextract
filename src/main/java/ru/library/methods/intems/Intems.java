package ru.library.methods.intems;

import ru.library.methods.KeywordExtraction;
import ru.library.text.word.Word;

import java.util.*;
import java.util.stream.Collectors;

public class Intems implements KeywordExtraction {

    private List<Word> words;

    public Intems() {
    }

    @Override
    public List<Word> extractKeywords(List<Word> words, Integer numberOfKeywords) throws Exception {

        if (words == null || words.size() == 0) {
            throw new Exception("Список слов пуст");
        }

        this.words = words;

        List<WordForIntems> list = calculateFrequencyForEachUniqueWord();
        calculateQWeigth(list);
        calculateFWeight(list);
        list = calculateIntems(list);

        List<Word> keywords = new ArrayList<>();

        for (int i = 0; i < numberOfKeywords; i++) {
            try {
                if (list.get(i).getScope() < 0) {
                    return keywords;
                }
                keywords.add(list.get(i));
            } catch (Exception ex) {
                return keywords;
            }
        }

        return keywords;
    }

    /**
     * Нахождениче частотности каждого уникального слова
     */
    private List<WordForIntems> calculateFrequencyForEachUniqueWord() {

        Map<String, WordForIntems> map = new HashMap<>();

        for (Word word : words) {
            if (map.containsKey(word.getWord())) {
                //WordForIntems tmp = map.get(word.getWord());
                WordForIntems tmp = map.get(word.getWord());
                tmp.setFrequency(tmp.getFrequency() + (float)(1.0 / words.size()));
                map.put(word.getWord(), tmp);
            } else {
                //WordForIntems tmp = new WordForIntems(word.getWord(), word.getPartOfSpeech(), word.getMorphCharateristics());
                WordForIntems tmp = new WordForIntems();
                tmp.setWord(word.getWord());
                tmp.setPartOfSpeech(word.getPartOfSpeech());
                tmp.setMorphCharateristics(word.getMorphCharateristics());
                tmp.setLength(word.getWord().length());
                if (word.getWord().length() == 0) {
                    System.out.println("!!!!!!!!!!!!!" + word.getWord());
                }
                tmp.setFrequency((float)(1.0 / words.size()));
                map.put(word.getWord(), tmp);
            }
        }

        List<WordForIntems> list = new ArrayList<>(map.values());
        //list = list.stream().sorted(Comparator.comparing(WordForIntems::getFrequency).reversed()).collect(Collectors.toList());
        return list;
    }

    /**
     * Подсчет параметра QWeight
     * @param words
     */
    private void calculateQWeigth(List<WordForIntems> words) {

        //Collections.sort(words, (o1, o2) -> o2.getFrequency() > o1.getFrequency() ? 1 : -1);
        List<WordForIntems> list = words.stream().sorted(Comparator.comparing(WordForIntems::getFrequency).reversed()).collect(Collectors.toList());

        for (int i = 0; i < list.size(); i++) {
            int count = 0;
            for (int j = i; j >= 0; j--) {
                if (list.get(j).getFrequency() >= list.get(i).getFrequency()) {
                    count++;
                }
            }
            WordForIntems tmp = list.get(i);
            tmp.setNumberOfWordsMoreThenThisFrequency(count);
            list.set(i, tmp);
            //words.get(i).setNumberOfWordsMoreThenThisFrequency(count);
        }

        list.forEach(word -> word.setqWeigth((list.size() - (float)word.getNumberOfWordsMoreThenThisFrequency()) / list.size()));
        words = list;
    }

    /**
     * Подсчет параметра FWeight
     * @param words
     */
    private void calculateFWeight(List<WordForIntems> words) {
        List<WordForIntems> list = words.stream().sorted(Comparator.comparing(WordForIntems::getLength).reversed()).collect(Collectors.toList());
        //Collections.sort(words, (o1, o2) -> o2.getWord().length() > o1.getWord().length() ? 1 : -1);

        for (int i = 0; i < list.size(); i++) {
            int count = 0;
            for (int j = i; j >= 0; j--) {
                if (list.get(j).getWord().length() >= list.get(i).getWord().length()) {
                    count++;
                }
            }
            WordForIntems tmp = list.get(i);
            tmp.setNumberOfWordsMoreThenThisLength(count);
            list.set(i, tmp);
        }

        list.forEach(word -> word.setfWeight((float) word.getNumberOfWordsMoreThenThisLength() / list.size()));
        words = list;
    }

    private List<WordForIntems> calculateIntems(List<WordForIntems> words) {
        words.forEach(word -> word.setScope(word.getqWeigth() - word.getfWeight()));
        return words.stream().sorted(Comparator.comparing(WordForIntems::getScope).reversed()).collect(Collectors.toList());
    }
}















