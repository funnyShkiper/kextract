package ru.library.methods.frequency;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.library.methods.KeywordExtraction;
import ru.library.text.word.Word;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Частотынй метод выделения ключевых слов из текстов.
 * Подсчитывается частота употребления слов в текса, сортируются в порядке убывания и возвращает указанное количество слов,
 * если оно не превышает общее их количество.
 */
public class RangFrequency implements KeywordExtraction {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    public RangFrequency() {
    }

    /**
     * Метод получения ключевых слов частотным методом
     * @param words список слов текста предоставляемых для обработки
     * @param numberOfKeywords количество возращаемых ключевых слов
     * @return список слов извлеченных при помощи частотного метода
     */
    @Override
    public List<Word> extractKeywords(List<Word> words, Integer numberOfKeywords) {

        List<Word> keywords = sortByFrequency(countWordsInText(words));
        List<Word> result = new ArrayList<>();

        if (numberOfKeywords == null) {
            for (int i = 0; i < keywords.size(); i++) {
                result.add(keywords.get(i));
            }
            return result;
        }

        try {
            for (int i = 0; i < numberOfKeywords; i++) {
                result.add(keywords.get(i));
            }
        } catch (Exception ex) {
            logger.info("Количество требуемых слов превышает объем доступных.");
        }

        return result;
    }

    /**
     * Подсчет частоты употребления всех слов текста
     * @param words список всех слов текста
     * @return уникальные слова текста, с подсчитаной частотой употребления
     */
    private Map<String, Word> countWordsInText(List<Word> words) {

        Map<String, Word> map = new HashMap<>();

        for (Word word : words) {
            if (map.containsKey(word.getWord())) {
                Word tmp = map.get(word.getWord());
                tmp.setScope(tmp.getScope() + (float)(1.0 / words.size()));
                map.put(word.getWord(), tmp);
            } else {
                Word wordInRangFrequencyMethod = new Word(
                  word.getWord(), word.getPartOfSpeech(), word.getMorphCharateristics(), (float) (1.0 / words.size())
                );
                map.put(wordInRangFrequencyMethod.getWord(), wordInRangFrequencyMethod);
            }
        }
        return map;
    }

    /**
     * Сортировка списка слов текста по убыванию частоты употребления
     * @param map уникальные слова текста, с подсчитаной частотой употребления
     * @return упорядоченный список слов текста по убыванию частоты употребления
     */
    private List<Word> sortByFrequency(Map<String, Word> map) {
        List<Word> list = new ArrayList<>();
        for (String key : map.keySet()) {
            list.add(map.get(key));
        }

        return list.stream()
                .sorted(Comparator.comparing(Word::getScope).reversed())
                .collect(Collectors.toList());
    }
}
