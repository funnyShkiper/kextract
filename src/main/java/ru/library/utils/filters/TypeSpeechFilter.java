package ru.library.utils.filters;

import ru.library.text.word.Word;

import java.util.List;

/**
 * Фильтрация списка слов текста {@link Word}
 */
public interface TypeSpeechFilter {

    /**
     * Фильтрация списка слов по части речи - существительное
     * @param words список слов класса {@link Word} для фильтрации
     * @return список существительных
     */
    List<Word> getNouns(List<Word> words);

    /**
     * Фильтрация списка слов по части речи - прилагательное
     * @param words список слов класса {@link Word} для фильтрации
     * @return список прилагательных
     */
    List<Word> getAdj(List<Word> words);

    /**
     * Фильтрация списка слов по части речи - существительное + прилагательное
     * @param words список слов класса {@link Word} для фильтрации
     * @return список существительных и прилагательных
     */
    List<Word> getNounsAndAdj(List<Word> words);
    
    /**
     * Фильтрация списка слов по части речи - глагол
     * @param words список слов класса {@link Word} для фильтрации
     * @return список глаголов
     */
    List<Word> getVerbs(List<Word> words);
    
    /**
     * Фильтрация списка слов по части речи - существительное + глагол
     * @param words список слов класса {@link Word} для фильтрации
     * @return список глаголов и существительных
     */
    List<Word> getNounsAndVerbs(List<Word> words);
    
    /**
     * Фильтрация списка слов по части речи - прилагательное + глагол
     * @param words список слов класса {@link Word} для фильтрации
     * @return список прилагательных и глаголов
     */
    List<Word> getAdjAndVerbs(List<Word> words);
    
    /**
     * Фильтрация списка слов по части речи - существительно, прилагательное и глагол
     * @param words список слов класса {@link Word} для фильтрации
     * @return список существительных, прилагательных и глаголов
     */
    List<Word> getNounsAdjVerbs(List<Word> words);

}
