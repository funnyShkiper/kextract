package ru.library.utils.filters;

import ru.library.text.word.Word;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Реализация интерфейса {@link TypeSpeechFilter}.
 */
public class TypeSpeechFilterImp implements TypeSpeechFilter{

    public TypeSpeechFilterImp() {
    }

    /**
     * Фильтрация списка слов по части речи - существительное
     * @param words список слов для фильтрации
     * @return список существительных
     */
    @Override
    public List<Word> getNouns(List<Word> words) {
        words = words.stream().filter(word -> word.getPartOfSpeech() == 17).collect(Collectors.toList());
        return words;
    }

    /**
     * Фильтрация списка слов по части речи - прилагательное
     * @param words список для для фильтрации
     * @return список прилагательных
     */
    @Override
    public List<Word> getAdj(List<Word> words) {
        words = words.stream().filter(word -> word.getPartOfSpeech() == 18).collect(Collectors.toList());
        return words;
    }

    /**
     * Фильтрация списка слов по части речи - существительное + прилагательное
     * @param words список для для фильтрации
     * @return список существительных и прилагательных
     */
    @Override
    public List<Word> getNounsAndAdj(List<Word> words) {
        words = words.stream().filter((word) -> word.getPartOfSpeech() == 17 || word.getPartOfSpeech() == 18).collect(Collectors.toList());
        return words;
    }

	@Override
	public List<Word> getVerbs(List<Word> words) {
		words = words.stream().filter(word -> word.getPartOfSpeech() == 20).collect(Collectors.toList());
		return words;
	}

	@Override
	public List<Word> getNounsAndVerbs(List<Word> words) {
		words = words.stream().filter(word -> word.getPartOfSpeech() == 17 || word.getPartOfSpeech() == 20).collect(Collectors.toList());
		return words;
	}

	@Override
	public List<Word> getAdjAndVerbs(List<Word> words) {
		words = words.stream().filter(word -> word.getPartOfSpeech() == 18 || word.getPartOfSpeech() == 20).collect(Collectors.toList());
		return words;
	}

	@Override
	public List<Word> getNounsAdjVerbs(List<Word> words) {
		words = words.stream().filter(word -> word.getPartOfSpeech() == 17 || word.getPartOfSpeech() == 18 || word.getPartOfSpeech() == 20).collect(Collectors.toList());
		return words;
	}
    
    

}
