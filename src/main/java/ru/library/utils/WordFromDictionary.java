package ru.library.utils;

import jmorfsdk.AllCharacteristicsOfForm;

import java.util.List;

public class WordFromDictionary {

    private List<AllCharacteristicsOfForm> allCharacteristicsOfForms;

    public WordFromDictionary() {
    }

    public WordFromDictionary(List<AllCharacteristicsOfForm> allCharacteristicsOfForms) {
        this.allCharacteristicsOfForms = allCharacteristicsOfForms;
    }

    public List<AllCharacteristicsOfForm> getAllCharacteristicsOfForms() {
        return allCharacteristicsOfForms;
    }

    public void setAllCharacteristicsOfForms(List<AllCharacteristicsOfForm> allCharacteristicsOfForms) {
        this.allCharacteristicsOfForms = allCharacteristicsOfForms;
    }

    @Override
    public String toString() {
        return "WordFromDictionary{" +
                "allCharacteristicsOfForms=" + allCharacteristicsOfForms +
                '}';
    }
}
