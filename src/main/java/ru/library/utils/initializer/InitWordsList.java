package ru.library.utils.initializer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ru.library.text.word.Word;
import ru.library.utils.WordFromDictionary;
import ru.library.utils.toservice.LoadLibrary;
import ru.library.utils.toservice.RequestToLocalLibrary;

/**
 * Класс {@link InitWordsList} предназначен для получения морфологических характеристик слов
 */
public class InitWordsList {

    private LoadLibrary requestToService;

    public InitWordsList() throws IOException {
        //requestToService = new RequestToService();
        requestToService = new RequestToLocalLibrary();
        //this.words = new ArrayList<>();
        //init(words);
    }

    /**
     * Генерация списка слов с морфологическими характеристиками из списка токенов
     * @param tokens токены выделенные из текста
     * @throws IOException
     */
    public List<Word> generateWordsList(List<String> tokens) throws IOException {
        List<Word> words = new ArrayList<>();
        for (String token : tokens) {
            WordFromDictionary wordFromDictionary = requestToService.getWordProperty(token);

            if (wordFromDictionary.getAllCharacteristicsOfForms().size() != 0) {

                if (wordFromDictionary.getAllCharacteristicsOfForms().get(0).getInitialFormString().equals("иза") /*||
                        wordFromDictionary.getAllCharacteristicsOfForms().get(0).getInitialFormString().equals("по") ||
                        wordFromDictionary.getAllCharacteristicsOfForms().get(0).getInitialFormString().equals("под")*/) {
                    continue;
                }

                Word tmpWord = new Word(wordFromDictionary.getAllCharacteristicsOfForms().get(0).getInitialFormString(),
                        wordFromDictionary.getAllCharacteristicsOfForms().get(0).getTypeOfSpeech(),
                        wordFromDictionary.getAllCharacteristicsOfForms().get(0).getMorfCharacteristics());

                words.add(tmpWord);
            }
        }
        return words;
    }

    /**
     * Генерация списка ключевых слов с морфологическими характеристиками, который заключен между тегами <keywords></keywords>
     * @param words
     * @return
     * @throws IOException
     */
    public List<Word> generateKeywordList(List<String> words) throws IOException {
        List<Word> list = new ArrayList<>();
        for (String word : words) {
            WordFromDictionary wordFromDictionary = requestToService.getWordProperty(word);

            if (wordFromDictionary.getAllCharacteristicsOfForms().size() != 0) {
                if (wordFromDictionary.getAllCharacteristicsOfForms().get(0).getTypeOfSpeech() == 17) {
                    Word tmpWord = new Word(wordFromDictionary.getAllCharacteristicsOfForms().get(0).getInitialFormString(),
                            wordFromDictionary.getAllCharacteristicsOfForms().get(0).getTypeOfSpeech(),
                            wordFromDictionary.getAllCharacteristicsOfForms().get(0).getMorfCharacteristics());

                    list.add(tmpWord);
                }
            }
        }
        return list;
    }

}
