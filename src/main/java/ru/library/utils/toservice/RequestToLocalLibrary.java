package ru.library.utils.toservice;

import ru.library.utils.WordFromDictionary;

import java.io.IOException;

public class RequestToLocalLibrary implements LoadLibrary {

    private LocalLoadMorphLibrary localLoadMorphLibrary;

    public RequestToLocalLibrary() {
        localLoadMorphLibrary = new LocalLoadMorphLibrary();
    }

    @Override
    public WordFromDictionary getWordProperty(String word) throws IOException {

        WordFromDictionary wordFromDictionary = new WordFromDictionary();
        wordFromDictionary.setAllCharacteristicsOfForms(localLoadMorphLibrary.getjMorfSdk().getAllCharacteristicsOfForm(word));
        return wordFromDictionary;
    }
}
