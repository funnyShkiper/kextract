package ru.library.utils.toservice;

import ru.library.utils.WordFromDictionary;

import java.io.IOException;

public interface LoadLibrary {

    WordFromDictionary getWordProperty(String word) throws IOException;

}
