package ru.library.utils.toservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jmorfsdk.JMorfSdk;
import jmorfsdk.load.LoadJMorfSdk;

/**
 * Локальная загрузка морфологической библиотеки ресского языка
 */
public class LocalLoadMorphLibrary {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private JMorfSdk jMorfSdk;

    public LocalLoadMorphLibrary() {
        try {
            logger.info("Старт загрузки библиотеки.");
            long startTime = System.nanoTime();
            jMorfSdk = LoadJMorfSdk.loadInAnalysisMode();
            long endTime = System.nanoTime();
            logger.info("Библиотека загружена. Время загрузки {} секунд", (endTime - startTime) / 1_000_000_000);
        } catch (Exception ex) {
            logger.error("Ошибка загрузки морфологической библиотеки.");
        }
    }

    public JMorfSdk getjMorfSdk() {
        return jMorfSdk;
    }
}
