package ru.library.utils.toservice;

import com.google.gson.Gson;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import ru.library.utils.WordFromDictionary;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class RequestToService implements LoadLibrary{

    private final String URL = "http://localhost:9999/api/word/";
    private HttpClient httpClient;
    private HttpGet request;

    public RequestToService() {

    }

    @Override
    public WordFromDictionary getWordProperty(String word) throws IOException {
        this.httpClient = HttpClientBuilder.create().build();
        try {
            request = new HttpGet(URL + word);
        } catch (Exception ex) {
            return null;
        }
        HttpResponse response = null;

        try {
            response = httpClient.execute(request);
        } catch (Exception ex) {
            return null;
        }

        Gson gSon = new Gson();

        System.out.println("Response code : " + response.getStatusLine().getStatusCode());

        BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = reader.readLine()) != null) {
            result.append(line);
        }

        return gSon.fromJson(result.toString(), WordFromDictionary.class);
    }
}
