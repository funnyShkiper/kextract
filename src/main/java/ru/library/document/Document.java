package ru.library.document;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import ru.library.text.tokenizer.TextTokenizer;
import ru.library.text.word.Word;

/**
 * {@link Document} представляет входной документ, используемый для дальнейшей
 * обработки
 * 
 * @author M.Ivashchenko
 * @version 1.0
 */
public class Document {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private String title;
	private String author;
	private String style;
	private String category;
	private List<String> keywords = new ArrayList<>();
	private String text;

	private List<String> tokens = new ArrayList<>();
	
	private TextTokenizer textTokenizer;

	public Document() {
		super();
	}

	public Document(File file) {
		try {
			parseFile(file);
			textTokenizer = new TextTokenizer(this.text, TextTokenizer.DELIM);
			initTokens();
			textTokenizer = new TextTokenizer(this.keywords.get(0), TextTokenizer.DELIM);
			initKeys();
		} catch (ParserConfigurationException e) {
			logger.error("Ошибка парсинга документа: " + file.getName());
		} catch (SAXException e) {
			logger.error("SAX parser ошибка");
		} catch (IOException e) {
			logger.error("Ошибка открытия документа: " + file.getName());
		} finally {
			System.out.println();
		}

	}

	public void initKeys() {
		keywords.clear();
		while (textTokenizer.hasMoreTokens()) {
			keywords.add(textTokenizer.nextToken().toLowerCase());
		}
	}

	private void initTokens() {
		while (textTokenizer.hasMoreTokens()) {
            tokens.add(textTokenizer.nextToken());
        }
	}

	/**
	 * Метод преобразования xml файла в объект класса {@link Document}
	 * 
	 * @param file
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	private void parseFile(File file) throws ParserConfigurationException, SAXException, IOException {

		DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		org.w3c.dom.Document document = documentBuilder.parse(file);

		Node root = document.getDocumentElement();

		NodeList items = root.getChildNodes();
		for (int index = 0; index < items.getLength(); index++) {
			Node node = items.item(index);
			if (node.getNodeType() != Node.TEXT_NODE) {
				switch (node.getNodeName()) {
				case "category":
					this.category = node.getTextContent().trim();
					break;
				case "style":
					this.style = node.getTextContent().trim();
					break;
				case "author":
					this.author = node.getTextContent().trim();
					break;
				case "title":
					this.title = node.getTextContent().trim();
					break;
				case "keywords":
					//this.keywords = convertStringToList(node.getTextContent().trim());
					this.keywords.add(node.getTextContent().trim());
					break;
				case "text":
					this.text = node.getTextContent().trim().toLowerCase();
					break;
				default:
					throw new RuntimeException("Ошибка чтения " + file.getName());
				}
			}
		}

	}

	/*private List<String> convertStringToList(String string) {
		List<String> list = Arrays.asList(string.split(","));
		for (int i = 0; i < list.size(); i++) {
			list.set(i, list.get(i).trim().toLowerCase());
		}
		return list;
	}*/

	public List<String> getTokens() {
		return tokens;
	}

	public void setTokens(List<String> tokens) {
		this.tokens = tokens;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public List<String> getKeywords() {
		return keywords;
	}

	public void setKeywords(List<String> keywords) {
		this.keywords = keywords;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return "Document [title=" + title + ", author=" + author + ", style=" + style + ", category=" + category
				+ ", keywords=" + keywords + ", text=" + text + "]";
	}

}
