package ru.library.text.word;

/**
 * Класс {@link Word} представляет слово с его характеристиками.
 * 
 * @author Max
 * @version 1.0
 */
public class Word {

	private String word;
	private byte partOfSpeech;
	private long morphCharateristics;
	private float scope;

	public Word() {
	}

	public Word(String word) {
		this.word = word;
	}

	public Word(String word, byte partOfSpeech, long morphCharateristics, float scope) {
		this.word = word;
		this.partOfSpeech = partOfSpeech;
		this.morphCharateristics = morphCharateristics;
		this.scope = scope;
	}

	public Word(String word, byte partOfSpeech, long morphCharateristics) {
		super();
		this.word = word;
		this.partOfSpeech = partOfSpeech;
		this.morphCharateristics = morphCharateristics;
	}

	public Word(String word, float scope) {
		this.word = word;
		this.scope = scope;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public byte getPartOfSpeech() {
		return partOfSpeech;
	}

	public void setPartOfSpeech(byte partOfSpeech) {
		this.partOfSpeech = partOfSpeech;
	}

	public long getMorphCharateristics() {
		return morphCharateristics;
	}

	public void setMorphCharateristics(long morphCharateristics) {
		this.morphCharateristics = morphCharateristics;
	}

	public float getScope() {
		return scope;
	}

	public void setScope(float scope) {
		this.scope = scope;
	}

	@Override
	public String toString() {
		return "Word{" +
				"word='" + word + '\'' +
				", partOfSpeech=" + partOfSpeech +
				", morphCharateristics=" + morphCharateristics +
				", scope=" + scope +
				'}';
	}

}
