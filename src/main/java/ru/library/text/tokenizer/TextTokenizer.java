package ru.library.text.tokenizer;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * Токенизер текста. Разбивает исходный текст на синтаксические единицы.
 * <br>
 * Разделителями являются следующие символы: .-,!;:?"<>\
 * <br>
 * НЕ РАБОТАЕТ со словами пишущимися через дефис.
 * 
 * @author Maxim
 * @version 1.0
 */
public class TextTokenizer {
	
	private StringTokenizer stringTokenizer;
	private Long tokenNumber = 0L;
	private Long totalTokens = 0L;
	
	public static final String DELIM = " .-,!;:?\"<>[]1234567890\\%\n\t\r»«—()";
	
	private Map<String, String> substitudes = new HashMap<>();
	
	/**
	 * Конструктор класса {@link TextTokenizer}
	 * @param text текстовая строка
	 * @param delim строка разделитель
	 */
	public TextTokenizer(String text, String delim) {
		stringTokenizer = new StringTokenizer(text, delim);
	}
	
	/**
	 * Конструктор
	 * @param text текстова строка
	 * @param delim строка разделитель
	 * @param includeDelim флаг включения разделитей как токенов
	 */
	public TextTokenizer(String text, String delim, boolean includeDelim) {
		stringTokenizer = new StringTokenizer(text, delim, includeDelim);
	}
	
	/**
	 * Функция возвращения следующего токена строки
	 * @return следующий токен
	 */
	public String nextToken() {
		String sToken = stringTokenizer.nextToken();
		if (substitudes.containsKey(sToken.trim())) {
			sToken = substitudes.get(sToken.trim());
		}
		tokenNumber++;
		return sToken;
	}
	
	/**
	 * Функция проверки наличия оставшихня токенов
	 * @return логическое значение true, если ещё токен
	 */
	public boolean hasMoreTokens() {
		if (totalTokens == 0) {
			totalTokens = countTokens();
		}
		return (tokenNumber < totalTokens);
	}
	
	/**
     * Функция проверки наличия оставшихся токенов
     * @return логическое значение true, если имеется еще токен
     */
    public boolean hasMoreElements()
    {
        return hasMoreTokens();
    }
    
    /**
     * Функция получения следующего токена
     * @return текущий токен строки типа Object
     */
    public Object nextElement()
    {
        return nextToken();
    }
    
    /**
     * Функция определения количества токенов в строке
     * @return количество токенов
     */
    public Long countTokens()
    {
        return Long.valueOf(stringTokenizer.countTokens());
    }

	public String getDELIM() {
		return DELIM;
	}
}



















