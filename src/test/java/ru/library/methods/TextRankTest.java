package ru.library.methods;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.library.document.Document;
import ru.library.methods.textrank.TextRank;
import ru.library.text.tokenizer.TextTokenizer;
import ru.library.text.word.Word;
import ru.library.utils.filters.TypeSpeechFilter;
import ru.library.utils.filters.TypeSpeechFilterImp;
import ru.library.utils.initializer.InitWordsList;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TextRankTest {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private Document document;
    private File file;
    private TextTokenizer textTokenizer;
    private List<String> tokens = new ArrayList<>();
    private InitWordsList initWordsList;
    private List<Word> words;

    @Before
    public void init() throws IOException {
        /*file = new File("src/main/resources/document.xml");
        document = new Document(file);
        textTokenizer = new TextTokenizer(document.getText(), TextTokenizer.DELIM);
        while (textTokenizer.hasMoreTokens()) {
            tokens.add(textTokenizer.nextToken());
        }
        initWordsList = new InitWordsList();
        words = initWordsList.generateWordsList(tokens);*/

    	file = new File("src/main/resources/document.xml");
    	document = new Document(file);
    	initWordsList = new InitWordsList();
    	words = initWordsList.generateWordsList(document.getTokens());

    }

    @Test
    public void TextRankTest() throws IOException {
        TextRank textRank = new TextRank();
        List<Word> keywords = textRank.extractKeywords(words, 15);
        for (Word word : keywords) {
            logger.info("Word {}, score {}", word.getWord(), word.getScope());
        }
    }

    @Test
    public void TextRankWithFilterBefore() throws IOException {
        TypeSpeechFilter typeSpeechFilter = new TypeSpeechFilterImp();
        TextRank textRank = new TextRank();
        List<Word> keywords;

        logger.info("Only nouns");
        keywords = typeSpeechFilter.getNouns(words);
        keywords = textRank.extractKeywords(keywords, 15);
        for (Word word : keywords) {
            logger.info("Word {}, score {}", word.getWord(), word.getScope());
        }

        logger.info("Only adj");
        keywords = typeSpeechFilter.getAdj(words);
        keywords = textRank.extractKeywords(keywords, 15);
        for (Word word : keywords) {
            logger.info("Word {}, score {}", word.getWord(), word.getScope());
        }

        logger.info("Only nouns + adj");
        keywords = typeSpeechFilter.getNounsAndAdj(words);
        keywords = textRank.extractKeywords(keywords, 15);
        for (Word word : keywords) {
            logger.info("Word {}, score {}", word.getWord(), word.getScope());
        }
    }

    @Test
    public void TextRankWithFilterAfter() throws IOException {
        TypeSpeechFilter typeSpeechFilter = new TypeSpeechFilterImp();
        TextRank textRank = new TextRank();
        List<Word> keywords;

        logger.info("Only nouns");
        keywords = textRank.extractKeywords(words, 15);
        keywords = typeSpeechFilter.getNouns(keywords);
        for (Word word : keywords) {
            logger.info("Word {}, score {}", word.getWord(), word.getScope());
        }

        logger.info("Only adj");
        keywords = textRank.extractKeywords(words, 15);
        keywords = typeSpeechFilter.getAdj(keywords);
        for (Word word : keywords) {
            logger.info("Word {}, score {}", word.getWord(), word.getScope());
        }

        logger.info("Only nouns + adj");
        keywords = textRank.extractKeywords(words, 15);
        keywords = typeSpeechFilter.getNounsAndAdj(keywords);
        for (Word word : keywords) {
            logger.info("Word {}, score {}", word.getWord(), word.getScope());
        }
    }
}