package ru.library.methods;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.library.document.Document;
import ru.library.methods.frequency.RangFrequency;
import ru.library.text.tokenizer.TextTokenizer;
import ru.library.text.word.Word;
import ru.library.utils.filters.TypeSpeechFilter;
import ru.library.utils.filters.TypeSpeechFilterImp;
import ru.library.utils.initializer.InitWordsList;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class RangFrequencyTest {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private Document document;
    private File file;
    private TextTokenizer textTokenizer;
    private List<String> tokens = new ArrayList<>();
    private InitWordsList initWordsList;
    private List<Word> words;

    @Before
    public void init() throws IOException {
        file = new File("src/main/resources/document.xml");
        document = new Document(file);
        textTokenizer = new TextTokenizer(document.getText(), TextTokenizer.DELIM);
        while (textTokenizer.hasMoreTokens()) {
            tokens.add(textTokenizer.nextToken());
        }
        initWordsList = new InitWordsList();
        words = initWordsList.generateWordsList(tokens);
    }

    @Test
    public void RankFrequencyTest() {
        RangFrequency rangFrequency = new RangFrequency();
        List<Word> keywords = rangFrequency.extractKeywords(words, 10);
        for (Word word : keywords) {
            logger.info("Word {}, frequency {}", word.getWord(), word.getScope());
        }
    }

    @Test
    public void RankFrequenceTestWithFilterBefore() {
        TypeSpeechFilter typeSpeechFilter = new TypeSpeechFilterImp();
        RangFrequency rangFrequency = new RangFrequency();
        List<Word> keywords;

        logger.info("Only nouns");
        List<Word> nouns = typeSpeechFilter.getNouns(words);
        keywords = rangFrequency.extractKeywords(nouns, 10);
        for (Word word : keywords) {
            logger.info("Word {}, frequency {}", word.getWord(), word.getScope());
        }

        logger.info("Only adj");
        List<Word> adj = typeSpeechFilter.getAdj(words);
        keywords = rangFrequency.extractKeywords(adj, 10);
        for (Word word : keywords) {
            logger.info("Word {}, frequency {}", word.getWord(), word.getScope());
        }

        logger.info("Only nouns + adj");
        List<Word> nounsAndAdj = typeSpeechFilter.getNounsAndAdj(words);
        keywords = rangFrequency.extractKeywords(nounsAndAdj, 10);
        for (Word word : keywords) {
            logger.info("Word {}, frequency {}", word.getWord(), word.getScope());
        }
    }

    @Test
    public void RankFrequencyTestWithFilterAfter() {
        TypeSpeechFilter typeSpeechFilter = new TypeSpeechFilterImp();
        RangFrequency rangFrequency = new RangFrequency();
        List<Word> keywords = rangFrequency.extractKeywords(words, 10);

        logger.info("Only nouns");
        List<Word> result = typeSpeechFilter.getNouns(keywords);
        for (Word word : result) {
            logger.info("Word {}, frequency {}", word.getWord(), word.getScope());
        }

        logger.info("Only adj");
        result = typeSpeechFilter.getAdj(keywords);
        for (Word word : result) {
            logger.info("Word {}, frequency {}", word.getWord(), word.getScope());
        }

        logger.info("Only nouns + adj");
        result = typeSpeechFilter.getNounsAndAdj(keywords);
        for (Word word : result) {
            logger.info("Word {}, frequency {}", word.getWord(), word.getScope());
        }
    }
}
