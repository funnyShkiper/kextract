package ru.library.methods;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.library.document.Document;
import ru.library.methods.intems.Intems;
import ru.library.methods.intems.WordForIntems;
import ru.library.text.tokenizer.TextTokenizer;
import ru.library.text.word.Word;
import ru.library.utils.filters.TypeSpeechFilter;
import ru.library.utils.filters.TypeSpeechFilterImp;
import ru.library.utils.initializer.InitWordsList;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class IntemsTest {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private Document document;
    private File file;
    private TextTokenizer textTokenizer;
    private List<String> tokens = new ArrayList<>();
    private InitWordsList initWordsList;
    private List<Word> words;
    private List<Word> keys;

    @Before
    public void init() throws IOException {
        file = new File("src/main/resources/3eae82e5028350404567aeaa2f8790c6.xml");
        document = new Document(file);
        initWordsList = new InitWordsList();
        words = initWordsList.generateWordsList(document.getTokens());
        keys = initWordsList.generateKeywordList(document.getKeywords());

        logger.info("Keys");
        for (Word word : keys) {
            logger.info("Word: {}, PartOfSpeech: {}", word.getWord(), word.getPartOfSpeech());
        }
    }

    @Test
    public void test() throws Exception {

        Intems intems = new Intems();
        TypeSpeechFilter typeSpeechFilter = new TypeSpeechFilterImp();
        List<Word> result = new ArrayList<>();
        result = typeSpeechFilter.getNouns(words);
        result = intems.extractKeywords(result,30);

        for (Word word : result) {
            logger.info(word.getWord() + " " +
                            "QWeight: " + ((WordForIntems)word).getqWeigth() + " " +
                            "FWeight: " + ((WordForIntems)word).getfWeight() + " " +
                            "Intem: " + word.getScope() + "");
        }

    }

}














