package ru.library.methods;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.library.document.Document;
import ru.library.methods.textrank.TextRankWithIDF;
import ru.library.text.tokenizer.TextTokenizer;
import ru.library.text.word.Word;
import ru.library.utils.filters.TypeSpeechFilter;
import ru.library.utils.filters.TypeSpeechFilterImp;
import ru.library.utils.initializer.InitWordsList;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TextRankWithIDFTest {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private File file;
    private TextTokenizer textTokenizer;
    private InitWordsList initWordsList;
    private List<Word> words;

    private HashMap<String, List<Word>> documents = new HashMap<>();

    @Before
    public void init() throws IOException {
        File file1 = new File("src/main/resources/document.xml");
        File file2= new File("src/main/resources/document_1.xml");
        File file3 = new File("src/main/resources/document_2.xml");
        File file4 = new File("src/main/resources/document_3.xml");
        initWordsList = new InitWordsList();
        Document document_1 = new Document(file1);
        Document document_2 = new Document(file2);
        Document document_3 = new Document(file3);
        Document document_4 = new Document(file4);

        documents.put(document_1.getTitle(), getWords(document_1));
        documents.put(document_2.getTitle(), getWords(document_2));
        documents.put(document_3.getTitle(), getWords(document_3));
        documents.put(document_4.getTitle(), getWords(document_4));

    }

    private List<Word> getWords(Document document) throws IOException {
        List<Word> item = new ArrayList<>();
        List<String> tokens = new ArrayList<>();
        TextTokenizer textTokenizer = new TextTokenizer(document.getText(), TextTokenizer.DELIM);
        while (textTokenizer.hasMoreTokens()) {
            tokens.add(textTokenizer.nextToken());
        }
        words = initWordsList.generateWordsList(tokens);
        return words;
    }

    @Test
    public void test() throws IOException {

        TextRankWithIDF textRankWithIDF = new TextRankWithIDF();
        Map<String, List<Word>> result = textRankWithIDF.extractKeywords(documents, 15);
        for (String title : result.keySet()) {

            logger.info(title);
            logger.info("-----------------");
            for (Word word : result.get(title)) {
                logger.info("{} - {}", word.getWord(), word.getScope());
            }
        }
    }

    @Test
    public void testWithUseFilterBeforeExtract() throws IOException {

        TypeSpeechFilter typeSpeechFilter = new TypeSpeechFilterImp();

        for (String title : documents.keySet()) {
            List<Word> tmp = documents.get(title);
            tmp = typeSpeechFilter.getNouns(tmp);
            documents.put(title, tmp);
        }

        TextRankWithIDF textRankWithIDF = new TextRankWithIDF();
        Map<String, List<Word>> result = textRankWithIDF.extractKeywords(documents, 15);
        logger.info("Only nouns");
        for (String key : result.keySet()) {
            logger.info("--- {} ---", key);
            for (Word word : result.get(key)) {
                logger.info("{} - {}", word.getWord(), word.getScope());
            }
        }


    }

}
