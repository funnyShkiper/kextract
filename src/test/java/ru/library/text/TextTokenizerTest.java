package ru.library.text;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.library.document.Document;
import ru.library.text.tokenizer.TextTokenizer;

@SuppressWarnings({ "unused" })
public class TextTokenizerTest {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private File file;
	private Document document;
	private TextTokenizer textTokenizer;
	
	@Before
	public void init() {
		file = new File("src/main/resources/document.xml");
		document = new Document(file);
	}
	
	@Test
	public void textTokenizer() {
		textTokenizer = new TextTokenizer(document.getText(), TextTokenizer.DELIM);
		List<String> tokens = new ArrayList<>();
		
		while (textTokenizer.hasMoreTokens()) {
			tokens.add((String) textTokenizer.nextElement());
		}
		
		List<String> text = Arrays.asList("исходный", "текст", "документа");
		
		assertEquals(text, tokens);
	}
}
