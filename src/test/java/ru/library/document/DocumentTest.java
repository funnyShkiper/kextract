package ru.library.document;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import ru.library.document.Document;

public class DocumentTest {
	
	private Document document;
	
	private File file = new File("src/main/resources/document.xml");
	
	@Before
	public void createDocumentTest() {
		document = new Document(file);
		assertNotNull(document);
	}
	
	@Test
	public void checkDocumentFields() {
		Document document = new Document();
		document.setCategory("Категория");
		document.setStyle("Стиль");
		document.setAuthor("Автор");
		document.setTitle("Название");
		document.setKeywords(Arrays.asList("ключевое слова", "слова"));
		document.setText("Исходный текст документа");
		
		assertEquals(document.toString(), this.document.toString());
	}
}
