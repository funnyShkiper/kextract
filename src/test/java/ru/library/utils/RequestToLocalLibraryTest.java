package ru.library.utils;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.library.document.Document;
import ru.library.text.tokenizer.TextTokenizer;
import ru.library.utils.toservice.LoadLibrary;
import ru.library.utils.toservice.RequestToLocalLibrary;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class RequestToLocalLibraryTest {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private Document document;
    private File file;
    private TextTokenizer textTokenizer;
    private LoadLibrary requestToLocalLibrary;

    private List<String> tokens = new ArrayList<>();

    @Before
    public void initTest() {
        file = new File("src/main/resources/document.xml");
        document = new Document(file);
        textTokenizer = new TextTokenizer(document.getText(), TextTokenizer.DELIM);
        while (textTokenizer.hasMoreTokens()) {
            tokens.add(textTokenizer.nextToken());
        }
        requestToLocalLibrary = new RequestToLocalLibrary();
    }

    @Test
    public void test() throws IOException {
        for (String token : tokens) {
            logger.info("{}", requestToLocalLibrary.getWordProperty(token));
        }
    }

}
