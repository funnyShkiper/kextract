package ru.library.utils.filters;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.library.document.Document;
import ru.library.text.tokenizer.TextTokenizer;
import ru.library.text.word.Word;
import ru.library.utils.initializer.InitWordsList;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TypeSpeechFilterTest {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private Document document;
    private File file;
    private TextTokenizer textTokenizer;
    private List<String> tokens = new ArrayList<>();
    private InitWordsList initWordsList;
    private List<Word> words;

    @Before
    public void init() throws IOException {
        file = new File("src/main/resources/document_1.xml");
        document = new Document(file);
        textTokenizer = new TextTokenizer(document.getText(), TextTokenizer.DELIM);
        while (textTokenizer.hasMoreTokens()) {
            tokens.add(textTokenizer.nextToken());
        }
        initWordsList = new InitWordsList();
        words = initWordsList.generateWordsList(tokens);
    }

    @Test
    public void Test() {
        TypeSpeechFilter typeSpeechFilter = new TypeSpeechFilterImp();
        List<Word> nouns = typeSpeechFilter.getVerbs(words);
        logger.info("Verb");
        for (Word word : nouns) {
            logger.info("{}", word);
        }

        logger.info("NounsAndVerb");
        List<Word> adj = typeSpeechFilter.getNounsAndVerbs(words);
        for (Word word : adj) {
            logger.info("{}", word);
        }

        logger.info("VerbAndAdj");
        List<Word> nounsAndAdj = typeSpeechFilter.getAdjAndVerbs(words);
        for (Word word : nounsAndAdj) {
            logger.info("{}", word);
        }

        logger.info("All");
        List<Word> all = typeSpeechFilter.getNounsAdjVerbs(words);
        for (Word word : all) {
            logger.info("{}", word);
        }

    }

}
