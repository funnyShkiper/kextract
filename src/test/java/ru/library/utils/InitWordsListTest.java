package ru.library.utils;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.library.document.Document;
import ru.library.text.tokenizer.TextTokenizer;
import ru.library.text.word.Word;
import ru.library.utils.initializer.InitWordsList;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class InitWordsListTest {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private Document document;
    private File file;
    private TextTokenizer textTokenizer;
    private List<String> tokens = new ArrayList<>();
    private InitWordsList initWordsList;
    private List<Word> words;

    @Before
    public void init() throws IOException {
        file = new File("src/main/resources/document.xml");
        document = new Document(file);
        textTokenizer = new TextTokenizer(document.getText(), TextTokenizer.DELIM);
        while (textTokenizer.hasMoreTokens()) {
            tokens.add(textTokenizer.nextToken());
        }
        initWordsList = new InitWordsList();
        words = initWordsList.generateWordsList(tokens);
    }

    @Test
    public void initWordsList() {
        for (Word word : words) {
            logger.info(word.toString());
        }
    }

}
